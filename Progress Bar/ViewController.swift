//
//  ViewController.swift
//  Progress Bar
//
//  Created by gddstudent on 16/5/16.
//  Copyright © 2016 WeW. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let url = NSURL(string: "http://www.google.com")!
        let request = NSURLRequest(URL: url)
        webView.loadRequest(request)
    }
    @IBAction func buttonWasTapped(sender: AnyObject) {
        let alert:UIAlertController = UIAlertController(title: "Button Tapped!", message: "A button was tapped", preferredStyle: UIAlertControllerStyle.Alert)
        let action:UIAlertAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Default) {
            (UIAlertAction) -> Void in print("item selcted")
        }
        let action1:UIAlertAction = UIAlertAction(title: "Item 1", style: UIAlertActionStyle.Default) {
            (UIAlertAction) -> Void in print("item 1 selcted")
        }
        alert.addAction(action)
        alert.addAction(action1)
        self.presentViewController(alert, animated: true) { () -> Void in
            print("alert presented")
        }
    }
    @IBAction func sliderDidChange(sender: UISlider) {
        let percent:Float = sender.value / sender.maximumValue
        progressBar.progress = percent
    }

    @IBAction func segmentedChanged(sender: UISegmentedControl) {
        let index:Int = sender.selectedSegmentIndex
        let title:String = sender.titleForSegmentAtIndex(index)!
        print("selected item is \(title)")
    }
    
    @IBAction func switchChanged(sender: UISwitch) {
        if sender.on {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

